import io
import os
import sys
import mimetypes
import contextlib
import importlib
import urllib


def download(url, file_path=None, directory=None, force=False):
    """
    Download from a remote url to a local file

    If the full file_path is not provided, the directory where the file will
    be stored must be provided and the file name will be infered from the url

    Return the full file path
    """

    assert (file_path is None) != (directory is None), 'Either file_path or directory must be None'

    if file_path is None:
        file_path = os.path.join(directory, os.path.basename(url))

    if force or not os.path.exists(file_path):
        os.makedirs(os.path.dirname(file_path), exist_ok=True)
        info(f'Downloading {url} ...')
        urllib.request.urlretrieve(url, file_path)
        info(f'Download completed ({file_path})')

    return file_path


def relax_equals(s1, s2):
    return normalize_str(s1) == normalize_str(s2)

def normalize_str(s):
    if not s:
        return None
    return s.strip().lower().replace(' ', '_')


def err(*args, **kwargs):
    print('ERROR:', *args, file=sys.stderr, **kwargs)
    sys.exit(1)

def info(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def open_read(filename=None):
    return smart_open(filename, 'r')


def open_write(filename=None):
    return smart_open(filename, 'w')


@contextlib.contextmanager
def smart_open(filename, mode):
    """
    Automatically detect compression based on file extension and yield the
    file handler (to use with 'with' statement)

    Two modes are supported: 'r' and 'w' (read and write, respectively)

    If filename is not provided or equals '-' (dash), use stdin for read mode
    and stdout for write mode.
    """

    mimetype_to_module = {
        'gzip': 'gzip',
        'bzip2': 'bz2',
        'xz': 'lzma',
    }

    if filename is None or filename == '-':
        if mode == 'r':
            fh = sys.stdin
        elif mode == 'w':
            fh = sys.stdout
        else:
            raise ValueError(f'Mode {mode} not supported')
    elif isinstance(filename, str) or isinstance(filename, os.PathLike):
        _, encoding = mimetypes.guess_type(filename)
        if encoding is None:
            opener = io.open
        else:
            module = mimetype_to_module[encoding]
            opener = importlib.import_module(module).open
        fh = opener(filename, f'{mode}t')
    else:
        yield filename
        return

    try:
        yield fh
    finally:
        if fh is not sys.stdin and fh is not sys.stdout:
            fh.close()
