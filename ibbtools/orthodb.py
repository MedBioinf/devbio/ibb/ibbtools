import requests
from collections import defaultdict
import os
import re
from . import util

SPECIES = 'Parasteatoda tepidariorum'

CONFIG = {
    'url': 'https://v101.orthodb.org/download/',
    'version': '10v1',
    'file': {
        'species': 'odb10v1_species.tab.gz',
        'genexref': 'odb10v1_gene_xrefs.tab.gz',
        'group2gene': 'odb10v1_OG2genes.tab.gz',
        'level': 'odb10v1_levels.tab.gz',
    },
    'xrefdb': {
        '7227': 'ENSEMBL', # Drosophila melanogaster
        '7070': 'ENSEMBL', # Tribolium castaneum
    } # Other species default to NCBIgid
}

def normalize_species_name(name):
    name = util.normalize_str(name)
    assert re.compile('[a-z]+_[a-z]+').match(name), 'Invalid species name'
    return name


def species_4letters(name: str):
    p1, p2 = name.split('_')
    return p1[0].upper() + p2[:3]


class Orthodb(object):
    name = 'orthodb'

    def __init__(self, species_names: list[str]):
        self.out_dir = 'data'
        self.cache_dir = os.path.join(self.out_dir, 'cache', self.name)
        self.species_names = [normalize_species_name(s) for s in species_names]
        self.config = CONFIG
        self.species_mapping = self.__get_species_mapping()

    def make_outfile(self, outfile=None):

        species_a, species_b = sorted(self.species_names[:2])
        if not outfile:
            outname = '_'.join([self.name, 'orthology'])
            outfile = os.path.join(self.out_dir, f'{outname}.tsv')

        util.info(f'Saving orthologs of {species_a} and {species_b} from {self.name} to {outfile}')

        with util.open_write(outfile) as f:
            print(f'OG\t{species_4letters(species_a)}\t{species_4letters(species_b)}', file=f)
            for group, orthologs in self._get_orthologs(species_a, species_b).items():
                orthologs_a = orthologs[species_a]
                orthologs_b = orthologs[species_b]
                if not orthologs_a or not orthologs_b:
                    continue
                print('\t'.join([group, ','.join(orthologs_a), ','.join(orthologs_b)]), file=f)
        util.info(f'Orthologs saved to {outfile}')


    def _get_orthologs(self, species_a, species_b):

        search_level_id = self.__get_level_id(species_a, species_b)
        mapping = self.__get_gene_xref_mapping()
        groups = defaultdict(lambda: defaultdict(set))

        with self._open_cache_or_download('group2gene') as f:
            for line in f:
                group, gene = line.strip().split('\t')[:2]
                if group.split('at')[1] != search_level_id:
                    continue

                species = gene.split(':')[0]
                for species_name in [species_a, species_b]:
                    if species in self.species_mapping[species_name]:
                        try:
                            mapped_gene = mapping[gene]
                        except KeyError:
                            # The id is not present in the xref db
                            pass
                        groups[group][species_name].add(mapped_gene)
        if not groups:
            util.err('No orthologs found')
        return groups


    def __get_gene_xref_mapping(self):
        mapping = {}
        search_species_ids = set(v for values in self.species_mapping.values()
                for v in values)

        util.info(f'Getting db xref mappings for identifiers...')
        with self._open_cache_or_download('genexref') as f:
            for line in f:
                orthodb_id, xref_id, xref_db = line.strip().split('\t')[:3]
                species = orthodb_id.split(':')[0]

                species_code = species.split('_')[0]
                if species_code in self.config['xrefdb']:
                    search_xref_db = self.config['xrefdb'][species_code]
                else:
                    search_xref_db = 'NCBIgid'
                if species in search_species_ids and xref_db == search_xref_db:
                    mapping[orthodb_id] = xref_id

        if not mapping:
            util.err('No mapping found.')

        util.info('Done.')
        return mapping


    def __get_level_id(self, species_a, species_b):
        """
        The ortho level can be specified in the config. E.g:

            orthodb:
                level: holometabola

        Or else, it will be infered as the lowest common ancestor of the
        two species.
        """

        if 'level' in self.config:
            level_name = self.config['level']

            with self._open_cache_or_download('level') as f:
                for line in f:
                    cols = line.strip().split('\t')
                    id_, name = cols[:2]
                    if util.relax_equals(name, level_name):
                        return id_
            util.err(f'Level "{level_name}" not found')
        else:
            species_tree = SpeciesTree()
            level_id = species_tree.get_lca(
                    next(iter(self.species_mapping[species_a])),
                    next(iter(self.species_mapping[species_b])))
            level_name = species_tree.get_species(level_id)['name']
            util.info(f'Using ortho level "{level_name}"')
            return level_id


    def __get_species_mapping(self):
        """Get mapping from species scientific names to orthodb internal
        species identifiers."""

        species_names = set(self.species_names)
        species_ids = defaultdict(set)

        with self._open_cache_or_download('species') as f:
            for line in f:
                cols = line.strip().split('\t')
                id_, name = cols[1:3]

                name = util.normalize_str(name)
                if name in species_names:
                    species_ids[name].add(id_)

        for name in species_names:
            if name not in species_ids:
                util.err(f'Species "{name}" not found')

        return species_ids
    
    def _open_cache_or_download(self, filetype):
        cached_file = os.path.join(self.cache_dir, self.config['file'][filetype])
        if not os.path.exists(cached_file):
            util.download(self.__get_remote_url(filetype), file_path=cached_file)
        return util.open_read(cached_file)

    def __get_remote_url(self, filetype):
        return self.config['url'] + self.config['file'][filetype]


class SpeciesTree:
    def __init__(self):
        tree_path = 'https://data.orthodb.org/current/tree'
        r = requests.get(tree_path)
        if not r.ok:
            util.err(f'Can not get "{tree_path}"')

        tree = r.json()['data']
        mapping = {}
        for species in self.__flatten_species_tree(tree):
            mapping[species['key']] = species

        self.mapping = mapping


    def get_species(self, species_id):
        return self.mapping[species_id]

    def get_ancestors(self, species_id):
        yield species_id
        try:
            species = self.mapping[species_id]
        except KeyError:
            return

        if 'parent' in species:
            yield from self.get_ancestors(species['parent'])


    def get_lca(self, species_a, species_b):
        """ Get lowest common ancestor """
        a_ancestors = list(self.get_ancestors(species_a))
        b_ancestors = set(self.get_ancestors(species_b))
        lca = next((x for x in a_ancestors if x in b_ancestors), None)
        return lca


    def __flatten_species_tree(self, tree):
        for species in tree:
            yield species
            subtree = species.get('children', [])
            yield from self.__flatten_species_tree(subtree)

if __name__ == '__main__':
    orthodb = Orthodb([SPECIES, 'Drosophila melanogaster'])
    orthodb.make_outfile()
