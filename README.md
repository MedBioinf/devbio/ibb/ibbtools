# ibbtools

CLI for iBeetle-Base. Python3 required.

To generate orthology for a species, for example *Parasteatoda tepidariorum*, using OrthoDB, run

```
python -m ibbtools.orthodb "Parasteatoda tepidariorum"
```

The generated file is `./data/orthodb_orthology.tsv`. After successfully running the command, you can delete the cache to save disk space:

```
rm -rf data/cache/
```
